﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Enter_Click(object sender, EventArgs e)
        {   
            string[] text = File.ReadAllLines("Users.txt");
            string userName = nameTXT.Text;
            string password = passwordTXT.Text;
            bool logIn = false;


            for (int i = 0; i < text.Length; i++)
                if (text[i].Split(',')[0] == userName && text[i].Split(',')[1] == password)
                {
                    logIn = true;
                    break;
                }

            if (!logIn)
            {
                MessageBox.Show("check youre user name or youre password");
                return;
            }

            this.Hide();
            birthday b = new birthday(userName);
            b.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
