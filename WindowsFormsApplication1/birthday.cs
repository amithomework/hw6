﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class birthday : Form
    {
        private string loginName;
        private Dictionary<string, string> dates;
        public birthday(string name)
        {
            loginName = name;
            FileStream F = new FileStream(loginName + "BD.txt", FileMode.OpenOrCreate);
            F.Close();

            dates = new Dictionary<string, string>();
            string[] text = File.ReadAllLines(loginName + "BD.txt");

            for (int i = 0; i < text.Length; i++)
            {
                string key = text[i].Split(',')[1].Split('/')[0] + text[i].Split(',')[1].Split('/')[1];
                string value = text[i].Split(',')[0]; 
                dates.Add(key, value);
            }
                
            
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string name = "";
            string date = monthCalendar1.SelectionRange.Start.ToShortDateString();

            try
            {
                name = dates[date.Split('/')[0] + date.Split('/')[1]];
                celebrateLbl.Text = name + " has birthday";
            }
            catch(Exception)
            {
                celebrateLbl.Text = "no one have birthday";
            }
            
        }

        private void add_Click(object sender, EventArgs e)
        {
            string newName = textBox1.Text; 
            textBox1.Text = "";
            string date = dateAdd.Value.ToShortDateString();
            string date2 =date.Split('/')[0] + date.Split('/')[1];
            MessageBox.Show(date2);
            try
            {
                string name = dates[date2];
                MessageBox.Show(name + " allready have birthday in this day");
            }
            catch (Exception)
            {
                
                StreamWriter F = new StreamWriter(loginName + "BD.txt");
                F.Write(newName + ',' + date);
                dates.Add(date2, newName);
            }
            
        }
    }
}
